<?php
namespace app\form;

use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class AdminForm extends Model
{
    const UPLOAD_DIR = 'upload/slider/';

    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg', 'maxFiles' => 8],
        ];
    }

    public function attributeLabels()
    {
        return [
            'imageFiles' => \Yii::t('app', 'Слайди'),
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $file->saveAs(self::UPLOAD_DIR . uniqid('MyApp', true) . '.' . $file->extension);//$file->baseName
            }
            return true;
        } else {
            return false;
        }
    }

    public function getFiles()
    {
        return FileHelper::findFiles(self::UPLOAD_DIR);
    }

    public function deleteFile($filePath)
    {
        FileHelper::unlink($filePath);
    }
}