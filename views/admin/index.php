<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="container">
    <?php
    echo Html::beginForm(['/user/logout'], 'post')
        . Html::submitButton(
            'Вийти (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-danger logout m3']
        )
        . '<a href="' . Url::to(['site/index']) . '" class="btn btn-primary m3">Головна сторінка</a>'
        . Html::endForm();
    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <div class="row">
        <?php
        foreach ($settings as $index => $setting) {
            echo '<div class="col-md-3">' . $form->field($setting, "[$index]value")->label($setting->name) . '</div>';
        }
        ?>
    </div>
    <div class="row">
    <?php
    $i=0;
    foreach($model->getFiles() as $filePath) {
        ?>
        <div class="col-md-3 form-group">
            <div class="slide-preview" style="background-image: url(<?= '/' . $filePath ?>)">
                <label class="delete-slide">
                    <input type="checkbox" name="File[removeFile][<?= $i ?>]" value="<?= $filePath ?>">
                </label>
            </div>
        </div>
        <?php
        $i++;
    }
    echo '</div><div class="row">';
    echo '<div class="col-md-3">' . $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) . '</div>';
    echo '</div>';
    echo '<button type="submit" class="btn btn-success">Зберегти</button>';
    ActiveForm::end();
    ?>
</div>