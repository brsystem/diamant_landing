<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
?>

<div class="main-slider-wrap">
    <div class="slider">
        <?php
        foreach($slides as $slide) {
            echo '<div class="slide-item" style="background-image: url(/' . $slide . ')"></div>';
        }
        ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-6 col-md-offset-8">
                <div class="contact-form-wrap blue-gradient-bg">
                    <div class="contact-form-label text-center form-group">
                        <?=Yii::t('app', 'СВЯЖИТЕСТЬ С НАМИ')?>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')
                        ->textInput(['placeholder' => $model->getAttributeLabel( 'name' )])
                        ->label(false) ?>

                    <?= $form->field($model, 'email')
                        ->textInput(['placeholder' => $model->getAttributeLabel( 'email' )])
                        ->label(false) ?>

                    <?= $form->field($model, 'phone')
                        ->textInput(['placeholder' => $model->getAttributeLabel( 'phone' )])
                        ->label(false) ?>

                    <?= $form->field($model, 'body')
                        ->textarea(['rows' => 6, 'placeholder' => $model->getAttributeLabel( 'body' )])
                        ->label(false)?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-xs-6">{image}</div><div class="col-xs-6">{input}</div></div>',
                    ]) ?>

                    <div class="text-center btn-contact-wrap">
                        <?= Html::submitButton(Yii::t('app', 'ОТПРАВИТЬ'), ['class' => 'btn btn-contact', 'name' => 'contact-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="second-slider-wrap">
    <div class="block-title text-blue text-center">
        <?=Yii::t('app', 'Радехівський цукор')?>
    </div>
    <div class="slider">
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
    </div>
</div>
<div class="container">
    <div class="propose-wrap text-center">
        <div class="block-title text-blue">
            <?=Yii::t('app', 'КРУПНЫЙ ПРОИЗВОДИТЕЛЬ КРИСТАЛЛИЧЕСКОГО САХАРА ПРЕДЛАГАЕТ')?>
        </div>
        <div class="propose-items text-left">
            <div class="propose-item">
                <div class="propose-icon"></div>
                <div class="propose-text">
                    <?=Yii::t('app', 'Сахар белый кристаллический свекольный насыпью, фасованный по 50 кг')?>
                </div>
            </div>
            <div class="propose-item">
                <div class="propose-icon"></div>
                <div class="propose-text">
                    <?=Yii::t('app', 'Сахар белый кристаллический свекольный, фасованный в бумажную упаковку по 1-25 кг')?>
                </div>
            </div>
            <div class="propose-item">
                <div class="propose-icon"></div>
                <div class="propose-text">
                    <?=Yii::t('app', 'Сахар белый кристаллический в стиках')?>
                </div>
            </div>
            <div class="propose-item">
                <div class="propose-icon"></div>
                <div class="propose-text">
                    <?=Yii::t('app', 'Сахар пресованный в бумажной упаковке')?>
                </div>
            </div>
            <div class="propose-item">
                <div class="propose-icon"></div>
                <div class="propose-text">
                    <?=Yii::t('app', 'Сахар тростниковый в бумажной упаковке по 0.5 кг')?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="our-advantages text-center">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="block-title text-blue" data-scroll>
                    <?=Yii::t('app', 'НАШИ ПРЕИМУЩЕСТВА')?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4" data-scroll="toggle(.fromLeftIn, .fromLeftOut)">
                <div class="advantage-icon">
                    <img src="/image/advantage1-icon.png" width="100" height="100">
                </div>
                <div class="advantage-text">
                    <?=Yii::t('app', 'Крупнейший экспортер сахара в Украине. Экспортируем в Ливию, Шри-Ланку, Турцию, Азербайджан, Узбекистан, Молдову.')?>
                </div>
            </div>
            <div class="col-sm-4" data-scroll>
                <div class="advantage-icon">
                    <img src="/image/advantage2-icon.png" width="100" height="100">
                </div>
                <div class="advantage-text">
                    <?=Yii::t('app', 'Более 40 лет на рынке')?>
                </div>
            </div>
            <div class="col-sm-4" data-scroll="toggle(.fromRightIn, .fromRightOut)">
                <div class="advantage-icon">
                    <img src="/image/advantage3-icon.png" width="200" height="100">
                </div>
                <div class="advantage-text">
                    <?=Yii::t('app', 'Высокое качество продукции. Изготавливается из высококачественного сырья на современном оборудовании лучших  мировых производителей.')?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="contacts-block">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="card-shadow blue-gradient-bg contact-block1" data-scroll="toggle(.scaleDownIn, .scaleDownOut)">
                    <div class="contact1">
                        <?=Yii::t('app', 'ООО Радеховский сахар')?>
                    </div>
                    <div class="contact2">
                        <?=Yii::t('app', 'Львовская обл, Радеховский р-н')?>
                    </div>
                    <div class="contact3">
                        <?=Yii::t('app', 'с.Павлов, проспект Юности, 39')?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="card-shadow">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18034.03860369957!2d24.52343524103271!3d50.260249862052476!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47253af5dff87647%3A0x5fcee00a2fe95cfe!2z0J_QsNCy0LvRltCyLCDQm9GM0LLRltCy0YHRjNC60LAg0L7QsdC70LDRgdGC0YwsIDgwMjUw!5e0!3m2!1suk!2sua!4v1562069337875!5m2!1suk!2sua"
                            width="100%"
                            height="250"
                            frameborder="0"
                            style="border:0"
                            allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <div class="languages">
            <?=Yii::t('app', 'Общаемся на украинском, русском и английском языках')?>
        </div>
    </div>
    <div class="row contact-form-block">
        <div class="col-md-6 col-xs-12">
            <div class="card-shadow contact-form-wrap2" data-scroll="toggle(.scaleDownIn, .scaleDownOut)">
                <?php $form = ActiveForm::begin(['id' => 'contact-form2']); ?>

                <?= $form->field($model, 'name')
                    ->textInput(['placeholder' => $model->getAttributeLabel( 'name' )])
                    ->label(false) ?>

                <?= $form->field($model, 'email')
                    ->textInput(['placeholder' => $model->getAttributeLabel( 'email' )])
                    ->label(false) ?>

                <?= $form->field($model, 'phone')
                    ->textInput(['placeholder' => $model->getAttributeLabel( 'phone' )])
                    ->label(false) ?>

                <?= $form->field($model, 'body')
                    ->textarea(['rows' => 6, 'placeholder' => $model->getAttributeLabel( 'body' )])
                    ->label(false)?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-xs-6">{image}</div><div class="col-xs-6">{input}</div></div>',
                ]) ?>

                <div class="text-center btn-contact-wrap">
                    <?= Html::submitButton(Yii::t('app', 'ОТПРАВИТЬ'), ['class' => 'btn btn-contact', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="card-shadow contact-image" style="background-image: url('/image/3.jpg')" data-scroll="toggle(.scaleDownIn, .scaleDownOut)">
            </div>
        </div>
    </div>
</div>
<div class="second-slider-wrap">
    <div class="block-title text-blue text-center">
        <?=Yii::t('app', 'Радехівський цукор')?>
    </div>
    <div class="slider">
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
        <div class="slide-item" style="background-image: url('/image/slider/1447168645.jpg')"></div>
    </div>
</div>