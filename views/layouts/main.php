<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="header">
    <div class="container">
        <div class="col-md-1 col-xs-12">
            <div class="pull-left">
                <img src="/image/radehivsky-sugar.png" alt="<?= Html::encode($this->title) ?>">
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="header-title text-red text-center">
                <?=\Yii::t('app', 'САХАР ОТ ПРОИЗВОДИТЕЛЯ')?>
            </div>
        </div>
        <div class="col-md-3 col-xs-12 text-blue text-right">
            <div class="display-inline text-center">
                <div>
                    <div class="display-inline">
                        <?= (isset($this->params['settings']['contact_phone']) ? $this->params['settings']['contact_phone']->value : '') ?>
                    </div>
                    <div class="display-inline contact-icons">
                        <div class="icon-phone"></div>
                        <div class="icon-viber"></div>
                        <div class="icon-whatsapp"></div>
                    </div>
                </div>
                <div class="header-email">
                    <?= (isset($this->params['settings']['contact_email']) ? $this->params['settings']['contact_email']->value : '') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap">
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
