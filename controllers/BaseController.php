<?php

namespace app\controllers;

use app\models\Setting;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        $settings = Setting::find()->indexBy('alias')->all();
        $this->view->params['settings'] = $settings;
        return parent::beforeAction($action);
    }
}