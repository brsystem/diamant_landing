<?php

namespace app\controllers;

use app\form\AdminForm;
use app\models\Setting;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use Yii;

class AdminController extends BaseController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return Yii::$app->response->redirect(['user/login']);
                },
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'empty';
        $settings = Setting::find()->indexBy('id')->all();
        $adminForm = new AdminForm();
        if (Yii::$app->request->isPost) {
            $isSettingsSaved = true;
            $fileData = Yii::$app->request->post('File');
            if(!empty($fileData)) {
                foreach ($fileData['removeFile'] as $filePath) {
                    $adminForm->deleteFile($filePath);
                }
            }
            if (Model::loadMultiple($settings, Yii::$app->request->post()) && Setting::validateMultiple($settings)) {
                foreach ($settings as $setting) {
                    $isSettingsSaved = $setting->save(false);
                }
            }
            $adminForm->imageFiles = UploadedFile::getInstances($adminForm, 'imageFiles');
            $isUploaded = $adminForm->upload();
            if ($isSettingsSaved && $isUploaded) {
                Yii::$app->session->setFlash('success', 'Data saved successful');
                return $this->redirect(Url::to(['admin/index']));
            }
        }

        return $this->render('index', ['model' => $adminForm, 'settings' => $settings]);
    }
}