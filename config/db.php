<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=diamant-landing-mysql;dbname=diamant_landing',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',


    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
