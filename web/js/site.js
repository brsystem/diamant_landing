$(document).ready(function(){
    $('.second-slider-wrap .slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: '',
        nextArrow: ''
    });
    $('.main-slider-wrap .slider').slick({
        prevArrow: '',
        nextArrow: '',
        autoplay: true,
        autoplaySpeed: 2000
    });
    $(document).on('change', '.delete-slide > input', function () {
        if ($(this).prop('checked')) {
            $(this).closest('.form-group').hide();
        }
    });
});
document.addEventListener('DOMContentLoaded', function() {
    var trigger = new ScrollTrigger({
        addHeight: true,
        once: true
    });
});