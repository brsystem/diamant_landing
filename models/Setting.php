<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Setting extends ActiveRecord
{
    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return '{{setting}}';
    }

    public function rules()
    {
        return [
            [['name','value'], 'required'],
        ];
    }
}
